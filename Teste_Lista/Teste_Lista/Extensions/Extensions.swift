//
//  Extensions.swift
//  Teste_Lista
//
//  Created by Ruy de Ascencão Neto on 17/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

import Foundation
import UIKit

extension Int {
    func returnDiminutiveNumber() -> String {
        if (self < 1000) {
            return "\(self)"
        }  else if (self < 1000000) {
            return "\(self/1000) k"
        }
        return "\(self/1000000) M"
    }
}
