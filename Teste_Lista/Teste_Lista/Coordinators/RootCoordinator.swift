//
//  RootCoordinator.swift
//  Teste_Lista
//
//  Created by Ruy de Ascencão Neto on 13/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

import Foundation
import UIKit

protocol CoordinatorPattern: class {
    func start()
    var childCoordinators: [CoordinatorPattern] {get set}
}

protocol ListDetailsCoordinatorDelegate {
    func transitToDetails(repository: LanguageCellViewModel)
}

class RootCoordinator: CoordinatorPattern {
    var navController: UINavigationController
    var childCoordinators: [CoordinatorPattern] = []
    
    func start() {
        let vc = ListViewBuilder.makeController(delegate:self)
        self.navController.pushViewController(vc, animated: true)
    }
    
    
    init(nav: UINavigationController) {
        self.navController = nav
    }
    
}

extension RootCoordinator: ListDetailsCoordinatorDelegate {
    func transitToDetails(repository: LanguageCellViewModel) {
        self.navController.pushViewController(DetailsViewBuilder.makeController(repository: repository), animated: true)
    }
}
