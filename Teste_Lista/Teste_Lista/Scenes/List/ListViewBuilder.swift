//
//  ListViewBuilder.swift
//  Teste_Lista
//
//  Created by Ruy de Ascencão Neto on 13/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

import Foundation
import RxSwift
class ListViewBuilder {
    static func makeController(delegate: ListDetailsCoordinatorDelegate) -> ListViewController {
        let service    = RepositoryService()
        let disposeBag = DisposeBag()
        let vm = ListViewViewModel(service: service, delegate: delegate, disposeBag: disposeBag)
        let vc = ListViewController(withViewModel: vm, disposeBag: disposeBag)
        return vc
    }
}
