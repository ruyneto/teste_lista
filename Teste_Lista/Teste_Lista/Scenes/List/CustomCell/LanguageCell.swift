//
//  LanguageCell.swift
//  Teste_Lista
//
//  Created by Ruy de Ascencão Neto on 18/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

public class LanguageCell: UITableViewCell {
    
    var card: UIView = {
        let vw = UIView()
        vw.layer.cornerRadius = 5
        vw.backgroundColor = .white
        return vw
    }()
    
    var labelTitle: UILabel = {
        let label           = UILabel()
        label.textColor     = .black
        label.font          = UIFont(name: "HelveticaNeue-Bold", size: 18.0)
        label.numberOfLines = 2
        label.text          = "title"
        return label
    }()
    
    var labelOwner: UILabel = {
        let label = UILabel()
        return label
    }()
    
    var badgeStar: UIView = {
        var view = UIView()
        view.layer.cornerRadius = 15.0
        view.backgroundColor = .red
        return view
    }()
    
    var imageStar: UIImageView = {
        var view      = UIImage(named: "star")
        var imageView = UIImageView(image: view)
        return imageView
    }()
    
    var imageSwift: UIImageView = {
        var image      = UIImage(named: "swift")
        var imageView = UIImageView(image: image)
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 25
        
        return imageView
    }()
    
    var labelCountStar: UILabel = {
        var label     = UILabel()
        return label
    }()
    
}

extension LanguageCell {
    func setup(vm: LanguageCellViewModel) {
        setupSubviews()
        setupLayout()
        setupStyle()
        setupValues(vm: vm)
    }
    
    func setupSubviews() {
        self.addSubview(card)
        
        self.card.addSubview(labelTitle)
        self.card.addSubview(labelOwner)
        self.card.addSubview(badgeStar)
        self.card.addSubview(imageSwift)
        self.card.addSubview(labelCountStar)
        
        self.badgeStar.addSubview(imageStar)
        self.badgeStar.addSubview(labelCountStar)
    }
    
    func setupLayout() {
        
        labelTitle.snp.makeConstraints {
            cm in
            cm.top.equalToSuperview().inset(20)
            cm.left.equalTo(imageSwift.snp_rightMargin).offset(20)
            cm.right.equalToSuperview().inset(20)
        }
        
        badgeStar.snp.makeConstraints {
            cm in
            cm.width.equalTo(100)
            cm.height.equalTo(30)
            cm.top.equalTo(labelTitle.snp_bottomMargin).offset(20)
            cm.left.equalTo(imageSwift.snp_rightMargin).offset(20)
        }
        
        card.snp.makeConstraints {
            cm in
            cm.left.right.top.bottom.equalToSuperview().inset(10)
        }
        
        //Images
        imageStar.snp.makeConstraints {
            cm in
            cm.left.equalToSuperview().offset(10)
            cm.height.equalToSuperview().multipliedBy(0.9)
            cm.width.equalTo(imageStar.snp.height)
        }
        
        labelCountStar.snp.makeConstraints {
            cm in
            cm.right.equalToSuperview().offset(-10)
            cm.centerY.equalToSuperview()
        }
        
        imageSwift.snp.makeConstraints {
            cm in
            cm.centerY.equalToSuperview()
            cm.left.equalTo(20)
            cm.height.width.equalTo(50)
        }
    
    
        
        labelTitle.snp.makeConstraints {
            cm in
            
        }
    }
    
    func setupStyle() {
        self.selectionStyle  = .none
        self.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }
    
    func setupValues(vm: LanguageCellViewModel){
        self.labelTitle.text = vm.title
        self.labelCountStar.text = vm.stars.returnDiminutiveNumber()
    }
}

