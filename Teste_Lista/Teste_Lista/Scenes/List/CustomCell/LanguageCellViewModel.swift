//
//  LanguageCellViewModel.swift
//  Teste_Lista
//
//  Created by Ruy de Ascencão Neto on 14/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

import Foundation

class LanguageCellViewModel {
    var title    : String
    var stars    : Int
    var urlPhoto : String
    var ownerName: String
    init(title: String, stars: Int, urlPhoto: String, ownerName: String) {
        self.title     = title
        self.stars     = stars
        self.urlPhoto  = urlPhoto
        self.ownerName = ownerName
    }
}
