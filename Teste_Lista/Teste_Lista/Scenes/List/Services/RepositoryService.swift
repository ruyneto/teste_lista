//
//  RepositoryService.swift
//  Teste_Lista
//
//  Created by Ruy de Ascencão Neto on 16/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

import Foundation
import RxSwift

protocol RepositoryServiceDelegate {
        func getList(page:Int) -> Observable<Data>?
}

class RepositoryService: RepositoryServiceDelegate {
    func getList(page: Int) -> Observable<Data>? {
        guard let url = URL(string: "https://api.github.com/search/repositories?q=language:swift&sort=stars&page=\(page)") else {return nil}
        let urlRequest = URLRequest(url: url)
        return URLSession.shared.rx.data(request: urlRequest)
    }
}
