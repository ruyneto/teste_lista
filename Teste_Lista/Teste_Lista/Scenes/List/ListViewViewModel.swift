//
//  ListViewViewModel.swift
//  Teste_Lista
//
//  Created by Ruy de Ascencão Neto on 13/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay


class ListViewViewModel {
    var coordinatorDelegate: ListDetailsCoordinatorDelegate
    var list = BehaviorRelay<[LanguageCellViewModel]>(value: [])
    var page = 1
    var title = "Lista"
    var service: RepositoryServiceDelegate
    weak var disposeBag    : DisposeBag?
    weak var refreshControl: UIRefreshControl?
    init(
        service: RepositoryServiceDelegate,
        delegate: ListDetailsCoordinatorDelegate,
        disposeBag: DisposeBag
    ) {
        self.service = service
        self.coordinatorDelegate = delegate
        self.disposeBag = disposeBag
    }
}

extension ListViewViewModel {
    func setupOutputs(table: UITableView) {
        self.refreshControl = table.refreshControl
        guard let disposeBag = disposeBag else {return}
        list
            .bind(to: table.rx.items(cellIdentifier: "rcell", cellType: LanguageCell.self)) {
                row,element,cell in
                cell.setup(vm: element)
            }.disposed(by: disposeBag)
        
        table
            .refreshControl?.rx.controlEvent(.valueChanged).subscribe {
            evento in
                self.page = 1
                self.requisiteList(page: self.page, pullRequest: true)
            }.disposed(by: disposeBag)
        
        
        table
            .rx
            .modelSelected(LanguageCellViewModel.self)
            .subscribe {
            item in
                
                guard let element = item.element else {return}
                self.coordinatorDelegate.transitToDetails(repository: element)
            }.disposed(by: disposeBag)
        
        requisiteList(page: self.page, pullRequest: true)
        
        table
            .rx
            .willDisplayCell
            .subscribe{
                event in
                let row   = event.element?.indexPath.row
                let count = self.list.value.count
                if(row == (count - 1)){
                    self.page += 1
                    self.requisiteList(page: self.page, pullRequest: false)
                }
            }
            .disposed(by: disposeBag)
    }
    
    func requisiteList(page: Int, pullRequest: Bool){
        guard let disposeBag = self.disposeBag else {return}
        service
        .getList(page:page)?
        .asObservable()
            .subscribeOn(MainScheduler.instance)
        .subscribe {
            next in
            do{
                guard
                    let element = next.element else {return }
                let json = try JSONDecoder().decode(ListResponse.self, from: element)
                
                guard let items = json.items else {return}
                let listOfVM:[LanguageCellViewModel] = items.map {
                    item in
                    guard
                        let starsCount = item.stargazers_count,
                        let nameOfRepo = item.name,
                        let urlPhoto   = item.owner?.avatar_url,
                        let ownerName  = item.owner?.login
                        
                        else {return LanguageCellViewModel(title: "Nenhum", stars: 0, urlPhoto: "",ownerName: "")}
                    return LanguageCellViewModel(title: nameOfRepo, stars: starsCount, urlPhoto: urlPhoto,ownerName: ownerName)
                }
                
                if pullRequest {
                    self.list.accept(listOfVM)
                    DispatchQueue.main.async {
                        self.refreshControl?.endRefreshing()
                    }
                }
                else {
                    var lastList = self.list.value
                    lastList.append(contentsOf: listOfVM)
                    self.list.accept(lastList)
                }
            }
            catch let error{
                print(error)
                return
            }
            
        }.disposed(by: disposeBag)
    }
}
