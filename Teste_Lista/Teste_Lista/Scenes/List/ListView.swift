//
//  ListView.swift
//  Teste_Lista
//
//  Created by Ruy de Ascencão Neto on 13/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class ListView: UIView {
    var listTable: UITableView =  {
        let view = UITableView()
        view.backgroundColor = #colorLiteral(red: 0.5032627583, green: 0.001983444206, blue: 0.5005265474, alpha: 1)
        view.rowHeight = 150.0
        let rc = UIRefreshControl()
        rc.attributedTitle = .init(string: "Puxe para atualizar")
        view.separatorStyle = UITableViewCell.SeparatorStyle.none
        view.refreshControl = rc
        view.register(LanguageCell.self, forCellReuseIdentifier: "rcell")
        return view
    }()
}

extension ListView {
    func setup() {
        setupHierarchy()
        setupLayouts()
        setupStyles()
    }
    
    func setupHierarchy() {
        self.addSubview(listTable)
    }
    
    func setupLayouts() {
        listTable.snp.makeConstraints {
            cm in
            cm
            .left
            .right
            .top
            .bottom
            .equalToSuperview()
        }
    }
    
    func setupStyles() {
       
    }
}
