//
//  ListViewController.swift
//  Teste_Lista
//
//  Created by Ruy de Ascencão Neto on 13/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

import Foundation
import RxSwift
import UIKit
import RxCocoa
class ListViewController: UIViewController {
    private var disposeBag: DisposeBag?
    private var listView  : ListView = ListView()
    private var viewModel : ListViewViewModel
    
    init(withViewModel viewModel: ListViewViewModel,disposeBag: DisposeBag) {
        self.viewModel  = viewModel
        self.disposeBag = disposeBag
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        disposeBag = nil
    }
}

// MARK: - Cycle of Life
extension ListViewController {
    override func loadView() {
        self.view = listView
        listView.setup()
        setupNavigation()
        setupBindings()
    }
}

extension ListViewController {
    func setupNavigation() {
        self.title = viewModel.title
    }
    
    func setupBindings() {
        viewModel.setupOutputs(table: self.listView.listTable)
    }
    
}
