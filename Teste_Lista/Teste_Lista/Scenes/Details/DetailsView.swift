//
//  DetailsView.swift
//  Teste_Lista
//
//  Created by Ruy de Ascencão Neto on 18/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class DetailsView: UIView {
    
    let footerView: UIView = {
        let view = UIView()
        view.backgroundColor    = .white
        view.layer.cornerRadius = 10
        view.layer.zPosition    = 2
        return view
    } ()
    
    let imagePerfil: UIImageView = {
        let view = UIImageView(image: nil)
        view.clipsToBounds = true
        view.layer.cornerRadius = 50.0
        view.layer.zPosition = 3
        return view
    } ()
    
    let cicleView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 60.0
        view.backgroundColor = .white
        view.layer.zPosition = 3
        return view
    } ()
    
    let labelTitle: UILabel = {
        let label  = UILabel()
        label.font = label.font.withSize(14)
        label.textAlignment = .center
        label.text = "Nome do repositório"
        return label
    } ()
    
    let labelTitleValue: UILabel = {
        let label  = UILabel()
        label.font = label.font.withSize(18)
        label.textAlignment = .center
        label.text = ""
        return label
    } ()
    
    let labelOwnerName: UILabel = {
        let label  = UILabel()
        label.font = label.font.withSize(14)
        label.textAlignment = .center
        label.text = "Username do dono"
        return label
    } ()
    
    let labelOwnerNameValue: UILabel = {
        let label  = UILabel()
        label.font = label.font.withSize(18)
        label.textAlignment = .center
        label.text = "Username do dono"
        return label
    } ()
}

extension DetailsView {
    func setup() {
        setupHierarchy()
        setupLayouts()
        setupStyles()
    }
    
    func setupHierarchy() {
        addSubview(footerView)
        addSubview(imagePerfil)
        addSubview(cicleView)
        bringSubviewToFront(imagePerfil)
        bringSubviewToFront(imagePerfil)
        
        self.footerView.addSubview(labelTitle)
        self.footerView.addSubview(labelTitleValue)
        self.footerView.addSubview(labelOwnerName)
        self.footerView.addSubview(labelOwnerNameValue)
    }
    
    func setupLayouts() {
        
        self.footerView.snp.makeConstraints {
            cm in
            cm.left.right.equalToSuperview().inset(20)
            cm.height.equalToSuperview().multipliedBy(0.7)
            cm.bottom.equalToSuperview().inset(20)
        }
        
        self.imagePerfil.snp.makeConstraints {
            cm in
            cm.height.width.equalTo(100)
            cm.centerX.equalToSuperview()
            cm.centerY.equalTo(self.footerView.snp.top)
        }
        
        self.cicleView.snp.makeConstraints {
            cm in
            cm.center.equalTo(self.imagePerfil.snp.center)
            cm.width.height.equalTo(120)
        }
        
        self.labelTitle.snp.makeConstraints {
            cm in
            cm.top.equalToSuperview().inset(80)
            cm.left.right.equalToSuperview().inset(20)
        }
        
        self.labelTitleValue.snp.makeConstraints {
            cm in
            cm.top.equalTo(self.labelTitle.snp_topMargin).offset(10)
            cm.left.right.equalToSuperview().inset(20)
        }
        
        self.labelOwnerName.snp.makeConstraints {
            cm in
            cm.top.equalTo(self.labelTitleValue.snp_topMargin).offset(10)
            cm.left.right.equalToSuperview().inset(20)
        }
        
        self.labelOwnerNameValue.snp.makeConstraints {
            cm in
            cm.top.equalTo(self.labelOwnerName.snp_topMargin).offset(10)
            cm.left.right.equalToSuperview().inset(20)
        }
    }
    
    func setupStyles() {
        self.backgroundColor = .purple
    }
    
}

