//
//  DetailsViewController.swift
//  Teste_Lista
//
//  Created by Ruy de Ascencão Neto on 18/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

import UIKit
import RxSwift

class DetailsViewController: UIViewController {
    var disposeBag: DisposeBag? = DisposeBag()
    var viewModel: DetailsViewModel
    var ownView: DetailsView = {
        let view = DetailsView()
        view.setup()
        return view
    } ()
    
    init(withViewModel viewModel: DetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    deinit {
        disposeBag = nil
    }
}

extension DetailsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func loadView() {
        self.view = self.ownView
        guard let disposeBag = self.disposeBag  else {return}
        viewModel.setupBinding(disposeBag: disposeBag, image: self.ownView.imagePerfil.rx.image)
        
        self.title = viewModel.title
        self.ownView.labelTitleValue.text = viewModel.titleRepository
        self.ownView.labelOwnerNameValue.text = viewModel.ownerName
        
    }

}
