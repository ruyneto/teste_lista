//
//  DetailsViewModel.swift
//  Teste_Lista
//
//  Created by Ruy de Ascencão Neto on 18/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxRelay
import RxCocoa

class DetailsViewModel {
    var title       = "Detalhes do Repositório"
    var service     : DetailsServiceDelegate
    var imagePerfil = BehaviorRelay<UIImage?>(value: nil)
    private var repository  : LanguageCellViewModel
    var titleRepository: String {
        get{
            return repository.title
        }
    }
    
    var ownerName: String {
        get{
            return repository.ownerName
        }
    }
    
    init(
        service: DetailsServiceDelegate,
        repository: LanguageCellViewModel
    ) {
        self.service = service
        self.repository = repository
    }
}

extension DetailsViewModel {
    private func loadImagePerfil(disposeBag:DisposeBag) {
        service.getPicture(url: repository.urlPhoto)?.observeOn(MainScheduler.asyncInstance).subscribe {
            onNext in
            guard
                let element = onNext.element,
                let image = UIImage(data: element)
                else {return}
            self.imagePerfil.accept(image)
        }.disposed(by: disposeBag)
    }
    
    func setupBinding(disposeBag: DisposeBag, image: Binder<UIImage?>){
        imagePerfil.bind(to: image).disposed(by: disposeBag)
        loadImagePerfil(disposeBag: disposeBag)
    }
}
