//
//  DetailsService.swift
//  Teste_Lista
//
//  Created by Ruy de Ascencão Neto on 18/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UIKit

protocol DetailsServiceDelegate {
    func getPicture(url:String) -> Observable<Data>?
}

class DetailsService: DetailsServiceDelegate {
    func getPicture(url:String) -> Observable<Data>? {
        guard let url = URL(string: url) else {return nil}
        let urlRequest = URLRequest(url: url)
        return URLSession.shared.rx.data(request: urlRequest)
    }
}
