//
//  DetailsBuilder.swift
//  Teste_Lista
//
//  Created by Ruy de Ascencão Neto on 18/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

import Foundation
import UIKit

class DetailsViewBuilder {
    static func makeController(repository: LanguageCellViewModel) -> DetailsViewController {
        let service = DetailsService()
        let vm = DetailsViewModel(service: service, repository: repository)
        let vc = DetailsViewController(withViewModel: vm)
        return vc
    }
}
