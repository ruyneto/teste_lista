//
//  DetailsServiceTest.swift
//  Teste_ListaTests
//
//  Created by Ruy de Ascencão Neto on 18/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

@testable import Teste_Lista
import XCTest
import Nimble
import Quick
import RxSwift
import SnapKit

class DetailsServiceTest: QuickSpec {
    
    override func spec() {
        let vc = DetailsService()
        describe("Teste do Details Service") {
            it("Uma url invalida deve gerar nil") {
                expect(vc.getPicture(url: "")).to(beNil())
            }
            
            it("Uma url valida deve gerar noTnil") {
                expect(vc.getPicture(url: "www.google.com.br")).notTo(beNil())
            }
        }
    }
}
