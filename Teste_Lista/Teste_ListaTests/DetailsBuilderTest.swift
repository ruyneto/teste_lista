//
//  DetailsBuilderTest.swift
//  Teste_ListaTests
//
//  Created by Ruy de Ascencão Neto on 18/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

@testable import Teste_Lista
import XCTest
import Nimble
import Quick
import RxSwift
import SnapKit

class DetailsBuilderTest: QuickSpec {
        var vm   = LanguageCellViewModel(title: "titulo", stars: 5, urlPhoto: "teste", ownerName: "dono")
    
    override func spec() {
        var vc = DetailsViewBuilder.makeController(repository: vm)
       describe("Teste do Details Builder") {
            it("O titulo deve ser") {
                expect(vc.ownView).notTo(beNil())
            }
        }
    }
}
