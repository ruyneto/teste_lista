//
//  DetailViewControllerTest.swift
//  Teste_ListaTests
//
//  Created by Ruy de Ascencão Neto on 18/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//

@testable import Teste_Lista
import XCTest
import Nimble
import Quick
import RxSwift
import SnapKit

class DetailViewControllerTest: QuickSpec {
    
    override func spec() {
        let service = DetailsService()
        let lmvm    = LanguageCellViewModel(title: "titulo", stars: 5, urlPhoto: "teste", ownerName: "dono")
        let vm = DetailsViewModel(service: service, repository: lmvm)
        let vc = DetailsViewController(withViewModel: vm)
        vc.viewDidLoad()
        vc.loadView()
    }
}
