//
//  ExtensionTest.swift
//  Teste_ListaTests
//
//  Created by Ruy de Ascencão Neto on 18/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//


import XCTest
import Nimble
import Quick
@testable import Teste_Lista

class ExtensionTest: QuickSpec {
  override func spec() {
    describe("Teste da extensão para reduzir numeros") {
      it("deve funcionar para o caso de 50000") {
        let numero = 50000.returnDiminutiveNumber()
        expect(numero).to(equal("50 k"))
    }

        it("deve funcionar para o caso de 999") {
            let numero = 999.returnDiminutiveNumber()
            expect(numero).to(equal("999"))
        }

        it("deve funcionar para o caso de 1000000") {
            let numero = 1000000.returnDiminutiveNumber()
            expect(numero).to(equal("1 M"))
        }
    }
  }
}
