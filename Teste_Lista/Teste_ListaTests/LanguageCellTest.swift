//
//  LanguageCell.swift
//  Teste_ListaTests
//
//  Created by Ruy de Ascencão Neto on 18/10/20.
//  Copyright © 2020 Ruy Neto. All rights reserved.
//
@testable import Teste_Lista
import XCTest
import Nimble
import Quick
import RxSwift
import SnapKit

class LanguageCellTest: QuickSpec {
    var cell = LanguageCell()
    var vm   = LanguageCellViewModel(title: "titulo", stars: 5, urlPhoto: "teste", ownerName: "dono")
    override func spec() {
        cell.setup(vm: vm)
       describe("Teste da Language Cell") {
            it("O titulo deve ser") {
                expect(self.cell.labelTitle.text).to(equal("titulo"))
            }
        }
    }
}
